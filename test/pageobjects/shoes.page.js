const Page = require('./page');

class ShoesPage extends Page {

//banner
    get banner() {
        return $('.d1img70');
    }

//bannerH2
    get bannerH2() {
        return this.banner.$('h2');
    }
    get bannerH2Text() {
        return this.bannerH2.getText();
    }

//bannerLink
    get bannerLink () {
        return this.banner.$('a');
    }
    get bannerLinkHref () {
        return this.bannerLink.getAttribute('href');
    }
    get bannetLinkTagName() {
        return this.bannerLink.getTagName();
    }

    bannerLinkClick() {
        return this.bannerLink.click();
    }

//page link
    open() {
        return super.open('b/Luxury-on-eBay/bn_7109710914?_trkparms=pageci%3A713107c1-247b-11ee-8c84-e601fb21ef0b%7Cparentrq%3A62f40f161890aab1078bf72bfffff5b4%7Ciid%3A0');
    }
}

module.exports = new ShoesPage();