const Page = require('./page');

class MainPage extends Page {

    get searchInput() {
        return $('#gh-ac');
    }
    get searchInputValue() {
        return this.searchInput.getValue();
    }
    get categorySelect() {
        return $('#gh-cat');
    }
    get categorySelectValue() {
        return this.categorySelect.$('option:nth-child(1)');
    }
    get categorySelectValueText() {
        return this.categorySelectValue.getText();
    }
    get searchBtn() {
        return $('#gh-btn');
    }

    async search(value) {
        await this.searchInput.setValue(value);
        await this.searchBtn.click();
    }

    open() {
        return super.open('');
    }
}

module.exports = new MainPage();