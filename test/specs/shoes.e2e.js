const ShoesPage = require('../pageobjects/shoes.page');

describe('Shoes page testing.', () => {
    it('Should open test page and verify the title', async() => {
        await ShoesPage.open();
        await expect(browser).toHaveTitle('Luxury on eBay products for sale | eBay');
    });

    it('Should check the banner is displayed', async() => {
        await expect(ShoesPage.banner).toBeExisting();
    });

    it('Should verify the banner title', async() => {
        await expect(ShoesPage.bannerH2).toHaveText('Collectible Sneakers');
    });

    it('Should check if link containing a text', async() => {
        await expect(ShoesPage.bannerLink).toHaveLinkContaining('/Retail-Campaign/');
    });

    it('Should check if a link on a banner is clickable', async() => {
        await expect(ShoesPage.bannerLink).toBeClickable();
    });

    it('Should open a new URL by clicking on a button', async() => {
        await ShoesPage.bannerLinkClick();
        await expect(browser).toHaveUrl('https://www.ebay.com/b/Retail-Campaign/bn_7116433159');
    });
});