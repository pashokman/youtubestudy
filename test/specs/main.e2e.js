const MainPage = require('../pageobjects/main.page')

describe('Main page testing.', () => {
    it('Should open the main page and verify the title', async () => {
        await MainPage.open();
        await expect(browser).toHaveTitle(
            'Electronics, Cars, Fashion, Collectibles & More | eBay');
    });

    it('Should search for a product and verify  the search text value', async () => {
        await MainPage.search('Laptop');      
        await expect(MainPage.searchInput).toHaveValue('Laptop');       
    });

    it('Should redirect to a new page and verify the title', async() => {
        await expect(browser).toHaveTitle('Laptop for sale | eBay');
    });

    it('Should update the search category', async() => {
        await expect(MainPage.categorySelectValue).toHaveText('PC Laptops & Netbooks');         
    });
})

