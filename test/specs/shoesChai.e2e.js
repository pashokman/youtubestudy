import { expect as chaiExpect} from 'chai'; 

const ShoesPage = require('../pageobjects/shoes.page');

describe('Shoes page testing', () => {
    it('Should open test page and verify the title', async() => {
        await ShoesPage.open();
        //await expect(browser).toHaveTitle('Luxury on eBay products for sale | eBay');
        chaiExpect(await ShoesPage.pageTitle).to.equal('Luxury on eBay products for sale | eBay')
    });

    it('Should check the banner is displayed', async() => {
        await expect(ShoesPage.banner).toBeExisting();
    });

    it('Should check the banner title is not empty', async() => {
        //await expect(ShoesPage.bannerH2).toHaveText('Collectible Sneakers');
        chaiExpect(await ShoesPage.bannerH2Text).to.not.be.empty;
    });

    it('Should verify the banner title', async() => {
        //await expect(ShoesPage.bannerH2).toHaveText('Collectible Sneakers');
        chaiExpect(await ShoesPage.bannerH2Text).to.include('Collectible Sneakers');
        
    });

    it('Should check if link containing a text', async() => {
        //await expect(ShoesPage.bannerLink).toHaveLinkContaining('/Retail-Campaign/');
        chaiExpect(await ShoesPage.bannerLinkHref).to.include('/Retail-Campaign/');
    });

    it('Should check the banner button is a tag <a>', async() => {
        chaiExpect(await ShoesPage.bannetLinkTagName).to.equal('a');
    });

    it('Should check if a link on a banner is clickable', async() => {
        await expect(ShoesPage.bannerLink).toBeClickable();
    });

    it('Should open a new URL by clicking on a button', async() => {
        await ShoesPage.bannerLinkClick();
        chaiExpect(await ShoesPage.pageUrl).to.equal('https://www.ebay.com/b/Retail-Campaign/bn_7116433159');
    });
});