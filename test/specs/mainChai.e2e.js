const MainPage = require('../pageobjects/main.page')
import { expect as chaiExpect} from 'chai';

describe('Main page testing', () => {
    it('Should open the main page and verify the title', async () => {
        await MainPage.open();
        //await expect(browser).toHaveTitle('Electronics, Cars, Fashion, Collectibles & More | eBay');
        chaiExpect(await MainPage.pageTitle).to.equal(
            'Electronics, Cars, Fashion, Collectibles & More | eBay');
        
    });

    it('Should search for a product and verify  the search text value', async () => {
        await MainPage.search('Laptop');      
        //await expect(MainPage.searchInput).toHaveValue('Laptop');   
        chaiExpect(await MainPage.searchInputValue).to.equal('Laptop');
            
    });

    it('Should redirect to a new page and verify the title', async() => {
        //await expect(browser).toHaveTitle('Laptop for sale | eBay');
        chaiExpect(await MainPage.pageTitle).to.equal('Laptop for sale | eBay');
    });

    it('Should update the search category', async() => {
        //await expect(MainPage.categorySelectValue).toHaveText('PC Laptops & Netbooks');         
        chaiExpect(await MainPage.categorySelectValueText).to.equal('PC Laptops & Netbooks');
    });
})

